package com.litegames.smarty.alpha.game.tictactoe;

import com.litegames.smarty.game.GamePlayer;
import com.litegames.smarty.game.GameState;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.Random;

public class TTTAIGamePlayer implements GamePlayer {

    private static final Logger logger = LoggerFactory.getLogger(TTTAIGamePlayer.class);

    private int id;

    private Random random;

    private Listener listener;

    public TTTAIGamePlayer(int id) {
        this.id = id;
        this.random = new Random();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void processGameState(GameState gs) {
        logger.debug("Received game state. mySelf: {}, gameState: {}", this, gs);

        TTTGameState gameState = (TTTGameState) gs;

        switch (gameState.playState) {
            case PLAYING: {
                TTTGame.Mark myMark = gameState.getPlayerState(id).mark;
                boolean myMove = gameState.currentMark.equals(myMark);
                if (myMove) {
                    performMove(myMark, gameState.board);
                }
                break;
            }

            case FINISHED: {
                gameState.playerQuit(getId());
                break;
            }
        }

        notifyGameStateChanged(gameState);
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "{%s id: %d}", getClass().getSimpleName(), getId());
    }

    private void performMove(TTTGame.Mark mark, TTTGame.Mark[][] board) {
        int numOfPossibleMoves = 0;

        for (int x = 0; x < TTTGame.BOARD_WIDTH; ++x) {
            for (int y = 0; y < TTTGame.BOARD_HEIGHT; ++y) {
                if (board[x][y] == null) {
                    numOfPossibleMoves++;
                }
            }
        }

        if (numOfPossibleMoves > 0) {
            int move = random.nextInt(numOfPossibleMoves);

            search_move:
            for (int x = 0; x < TTTGame.BOARD_WIDTH; ++x) {
                for (int y = 0; y < TTTGame.BOARD_HEIGHT; ++y) {
                    if (board[x][y] == null) {
                        if (move == 0) {
                            board[x][y] = mark;
                            break search_move;
                        }
                        move--;
                    }
                }
            }
        } else {
            logger.error("Failed to perform move. Can't find empty field on board.");
        }
    }

    private void notifyGameStateChanged(TTTGameState gameState) {
        logger.debug("Sending game state. mySelf: {}, gameState: {}", this, gameState);

        if (listener != null) {
            listener.onGamePlayerStateChange(this, gameState);
        }
    }

}
