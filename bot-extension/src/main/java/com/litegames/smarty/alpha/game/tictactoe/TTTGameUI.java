package com.litegames.smarty.alpha.game.tictactoe;

import java.util.List;

public interface TTTGameUI {

    interface EventListener {
        void onMove(TTTGameUI gameUI, int x, int y);

        void onQuit(TTTGameUI gameUI);
    }

    void showGameState(List<TTTGamePlayerInfo> gamePlayersInfos, TTTGameState gameState);

    void finish(List<TTTGamePlayerInfo> gamePlayersInfos, TTTGameState gameState);

    void performMove();

    void setEventListener(EventListener listener);

}
