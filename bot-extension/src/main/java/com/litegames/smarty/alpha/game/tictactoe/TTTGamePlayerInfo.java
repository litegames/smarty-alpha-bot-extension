package com.litegames.smarty.alpha.game.tictactoe;

public class TTTGamePlayerInfo {

    public int id;
    public String name;
    public Object avatar;
    public int color;
    public boolean mySelf;

}
