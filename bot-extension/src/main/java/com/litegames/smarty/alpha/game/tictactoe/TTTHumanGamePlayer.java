package com.litegames.smarty.alpha.game.tictactoe;

import com.litegames.smarty.game.GamePlayer;
import com.litegames.smarty.game.GameState;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Locale;

public class TTTHumanGamePlayer implements GamePlayer, TTTGameUI.EventListener {

    private static final Logger logger = LoggerFactory.getLogger(TTTHumanGamePlayer.class);

    private int id;

    private List<TTTGamePlayerInfo> gamePlayersInfos;

    private TTTGameUI gameUI;
    private boolean updateUI;
    private boolean handleEventsAlways;

    private TTTGameState gameState;

    private Listener listener;

    public TTTHumanGamePlayer(int id, List<TTTGamePlayerInfo> gamePlayersInfos, TTTGameUI gameUI, boolean updateUI, boolean handleEventsAlways) {
        this.id = id;

        this.gamePlayersInfos = gamePlayersInfos;

        this.gameUI = gameUI;
        this.updateUI = updateUI;
        this.handleEventsAlways = handleEventsAlways;

        this.gameState = null;

        if (handleEventsAlways) {
            gameUI.setEventListener(this);
        }
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void processGameState(GameState gs) {
        this.gameState = (TTTGameState) gs;

        logger.debug("Received game state. mySelf: {}, gameState: {}", this, gameState);

        switch (gameState.playState) {
            case PLAYING: {
                boolean myMove = gameState.currentMark.equals(gameState.getPlayerState(id).mark);

                if (myMove) {
                    if (!handleEventsAlways) {
                        gameUI.setEventListener(this);
                    }

                    if (updateUI) {
                        gameUI.showGameState(gamePlayersInfos, gameState);
                    }

                    gameUI.performMove();
                } else {
                    if (updateUI) {
                        gameUI.showGameState(gamePlayersInfos, gameState);
                    }
                    notifyGameStateChanged(gameState);
                }
                break;
            }

            case FINISHED:
                gameState.playerQuit(getId());

                if (updateUI) {
//                    gameUI.showGameState(gamePlayersInfos, gameState);

                    gameUI.finish(gamePlayersInfos, gameState);
                }

                notifyGameStateChanged(gameState);
                break;
        }

    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void onMove(TTTGameUI gameUI, int x, int y) {
        if (!handleEventsAlways) {
            gameUI.setEventListener(null);
        }

        if (gameState.currentMark == null || !gameState.currentMark.equals(gameState.getPlayerState(getId()).mark)) {
            throw new AssertionError("Should be my turn");
        }
        if (gameState.board[x][y] != null) {
            throw new AssertionError(String.format(Locale.US, "Mark is already present at (%d, %d) position.", x, y));
        }

        gameState.board[x][y] = gameState.getPlayerState(id).mark;

        if (updateUI) {
            gameUI.showGameState(gamePlayersInfos, gameState);
        }

        notifyGameStateChanged(gameState);
    }

    @Override
    public void onQuit(TTTGameUI gameUI) {
        if (gameState.isFinished()) {
            return;
        }

        if (!handleEventsAlways) {
            gameUI.setEventListener(null);
        }

        gameState.playerQuit(id);

        if (updateUI) {
            gameUI.showGameState(gamePlayersInfos, gameState);

            gameUI.finish(gamePlayersInfos, gameState);
        }

        notifyGameStateChanged(gameState);
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "{%s: id: %d}", getClass().getSimpleName(), getId());
    }

    private void notifyGameStateChanged(TTTGameState gameState) {
        logger.debug("Sending game state. mySelf: {}, gameState: {}", this, gameState);

        if (listener != null) {
            listener.onGamePlayerStateChange(this, gameState);
        }
    }

}
