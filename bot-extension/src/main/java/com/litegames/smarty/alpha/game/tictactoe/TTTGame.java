package com.litegames.smarty.alpha.game.tictactoe;

import com.litegames.smarty.game.Game;
import com.litegames.smarty.game.GamePlayer;
import com.litegames.smarty.game.GameState;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TTTGame implements Game {

    public static final int PLAYERS_COUNT = 2;

    public static final int BOARD_WIDTH = 3;
    public static final int BOARD_HEIGHT = 3;

    public static final long SCORE_FOR_WIN = 3;
    public static final long SCORE_FOR_LOSE = 0;
    public static final long SCORE_FOR_TIE = 1;

    public static final String LEADERBOARD_IDENTIFIER = "97F9BC60-19A6-11E5-B939-0800200C9A66";
    // Empty leaderboard
//    public static final String LEADERBOARD_IDENTIFIER = "CCC55B30-1BFD-11E5-B939-0800200C9A66";

    public enum Mark {
        CROSS,
        NOUGHT;

        public Mark opposite() {
            if (CROSS.equals(this)) {
                return Mark.NOUGHT;
            } else if (NOUGHT.equals(this)) {
                return CROSS;
            } else {
                return null;
            }
        }
    }

    public enum Result {
        WIN,
        TIE,
    }

    private static final int WINNING_SEQUENCES[][][] = new int[][][]{
            // rows
            new int[][]{new int[]{0, 0}, new int[]{1, 0}, new int[]{2, 0}},
            new int[][]{new int[]{0, 1}, new int[]{1, 1}, new int[]{2, 1}},
            new int[][]{new int[]{0, 2}, new int[]{1, 2}, new int[]{2, 2}},

            // columns
            new int[][]{new int[]{0, 0}, new int[]{0, 1}, new int[]{0, 2}},
            new int[][]{new int[]{1, 0}, new int[]{1, 1}, new int[]{1, 2}},
            new int[][]{new int[]{2, 0}, new int[]{2, 1}, new int[]{2, 2}},

            // diagonals
            new int[][]{new int[]{0, 0}, new int[]{1, 1}, new int[]{2, 2}},
            new int[][]{new int[]{2, 0}, new int[]{1, 1}, new int[]{0, 2}},
    };

    private TTTGameState gameState;

    public TTTGame(List<GamePlayer> gamePlayers) {
        List<Integer> playersIds = new ArrayList<>();
        for (GamePlayer gamePlayer : gamePlayers) {
            playersIds.add(gamePlayer.getId());
        }

        this.gameState = new TTTGameState(playersIds);

        int crossPlayerIdx = new Random().nextInt(gamePlayers.size());
        for (int i = 0; i < gameState.playersStates.length; i++) {
            this.gameState.playersStates[i].mark = (i == crossPlayerIdx) ? TTTGame.Mark.CROSS : TTTGame.Mark.NOUGHT;
        }
        this.gameState.currentMark = Mark.CROSS;
    }

    @Override
    public TTTGameState processPlayersGameStates(List<GameState> playersGameStates) {
        switch (gameState.playState) {
            case PLAYING: {
                int currentPlayerIdx = -1;
                for (int idx = 0; idx < gameState.playersStates.length; ++idx) {
                    if (gameState.currentMark.equals(gameState.playersStates[idx].mark)) {
                        currentPlayerIdx = idx;
                        break;
                    }
                }

                TTTGameState currentPlayerGameState = (TTTGameState) playersGameStates.get(currentPlayerIdx);

                gameState.board = copyBoard(currentPlayerGameState.board, gameState.board);
                gameState.currentMark = gameState.currentMark.opposite();

                if (playerWithMarkIsWinner(TTTGame.Mark.CROSS, gameState.board)) {
                    gameState.result = Result.WIN;
                    gameState.winnerId = gameState.idOfPlayerWithMark(Mark.CROSS);
                } else if (playerWithMarkIsWinner(TTTGame.Mark.NOUGHT, gameState.board)) {
                    gameState.result = Result.WIN;
                    gameState.winnerId = gameState.idOfPlayerWithMark(Mark.NOUGHT);
                } else if (numOfFieldsWithMark(null, gameState.board) == 0) {
                    gameState.result = TTTGame.Result.TIE;
                    gameState.winnerId = -1;
                }

                if (gameState.result != null) {
                    gameState.currentMark = null;
                    gameState.playState = TTTGameState.PlayState.FINISHED;
                }

                break;
            }

            case FINISHED: {
                break;
            }
        }

        return gameState;
    }

    @Override
    public void playerQuit(int playerId) {
        gameState.playerQuit(playerId);
    }

    @Override
    public boolean isFinished() {
        return gameState.playState.equals(TTTGameState.PlayState.FINISHED);
    }

    @Override
    public TTTGameState getGameState() {
        return gameState;
    }

    private Mark[][] copyBoard(Mark[][] fromBoard, Mark[][] toBoard) {
        for (int x = 0; x < BOARD_WIDTH; x++) {
            System.arraycopy(fromBoard[x], 0, toBoard[x], 0, BOARD_HEIGHT);
        }

        return toBoard;
    }

    private boolean playerWithMarkIsWinner(TTTGame.Mark mark, TTTGame.Mark[][] board) {
        boolean winner = false;

        for (int[][] winningSequence : WINNING_SEQUENCES) {
            boolean allFieldsChecked = true;

            for (int[] winningSequenceItem : winningSequence) {
                int x = winningSequenceItem[0];
                int y = winningSequenceItem[1];
                allFieldsChecked &= mark.equals(board[x][y]);
            }

            if (allFieldsChecked) {
                winner = true;
                break;
            }
        }

        return winner;
    }

    private int numOfFieldsWithMark(TTTGame.Mark mark, TTTGame.Mark board[][]) {
        int num = 0;

        for (int x = 0; x < TTTGame.BOARD_WIDTH; ++x) {
            for (int y = 0; y < TTTGame.BOARD_HEIGHT; ++y) {
                if ((mark == null && board[x][y] == null) || (mark != null && mark.equals(board[x][y]))) {
                    num++;
                }
            }
        }

        return num;
    }

}
