package com.litegames.smarty.alpha.game.tictactoe;

import com.litegames.smarty.game.GameState;
import com.litegames.smarty.game.GameStateSerializer;
import com.litegames.smarty.sdk.Data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;

public class TTTGameStateSerializer implements GameStateSerializer {

    public static final String KEY_PLAYER_STATE = "p%d";
    public static final String KEY_PLAY_STATE = "playState";
    public static final String KEY_CURRENT_PLAYER_MARK = "currentMark";
    public static final String KEY_BOARD = "board";
    public static final String KEY_RESULT = "result";
    public static final String KEY_WINNER_ID = "winnerId";
    public static final String KEY_QUITTER_ID = "quitterId";

    private PlayerStateSerializer playerStateSerializer = new PlayerStateSerializer();

    @Override
    public GameState gameStateFromData(Data data) {
        TTTGameState s = new TTTGameState();

        s.playersStates = new TTTGameState.PlayerState[TTTGame.PLAYERS_COUNT];
        for (int i = 0; i < TTTGame.PLAYERS_COUNT; ++i) {
            Data playerStateData = data.getData(String.format(Locale.US, KEY_PLAYER_STATE, i));
            s.playersStates[i] = playerStateSerializer.playerStateFromData(playerStateData);
        }
        s.playState = TTTGameState.PlayState.values()[data.getInt(KEY_PLAY_STATE)];
        {
            Integer ordinal = data.getInt(KEY_CURRENT_PLAYER_MARK);
            s.currentMark = (ordinal == null) ? null : TTTGame.Mark.values()[ordinal];
        }
        s.board = deserializeBoard(data.getIntArray(KEY_BOARD));
        {
            Integer ordinal = data.getInt(KEY_RESULT);
            s.result = (ordinal == null) ? null : TTTGame.Result.values()[ordinal];
        }
        s.winnerId = data.getInt(KEY_WINNER_ID);
        s.quitterId = data.getInt(KEY_QUITTER_ID);

        return s;
    }

    @Override
    public Data gameStateToData(GameState gameState) {
        TTTGameState gs = (TTTGameState) gameState;

        Data data = new Data();

        for (int i = 0; i < TTTGame.PLAYERS_COUNT; i++) {
            Data playerStateData = playerStateSerializer.playerStateToData(gs.playersStates[i]);
            data.putData(String.format(Locale.US, KEY_PLAYER_STATE, i), playerStateData);
        }
        data.putInt(KEY_PLAY_STATE, gs.playState.ordinal());
        data.putInt(KEY_CURRENT_PLAYER_MARK, gs.currentMark == null ? null : gs.currentMark.ordinal());
        data.putIntArray(KEY_BOARD, serializeBoard(gs.board));
        data.putInt(KEY_RESULT, (gs.result == null) ? null : gs.result.ordinal());
        data.putInt(KEY_WINNER_ID, gs.winnerId);
        data.putInt(KEY_QUITTER_ID, gs.quitterId);

        return data;
    }

    private static class PlayerStateSerializer {
        public static final String KEY_ID = "id";
        public static final String KEY_MARK = "mark";
        public static final String KEY_QUITTED = "quitted";

        public TTTGameState.PlayerState playerStateFromData(Data data) {
            TTTGameState.PlayerState p = new TTTGameState.PlayerState();

            p.id = data.getInt(KEY_ID);
            p.mark = TTTGame.Mark.values()[data.getInt(KEY_MARK)];
            p.quitted = data.getBool(KEY_QUITTED);

            return p;
        }

        public Data playerStateToData(TTTGameState.PlayerState ps) {
            Data data = new Data();

            data.putInt(KEY_ID, ps.id);
            data.putInt(KEY_MARK, ps.mark.ordinal());
            data.putBool(KEY_QUITTED, ps.quitted);

            return data;
        }
    }

    static Collection<Integer> serializeBoard(TTTGame.Mark[][] board) {
        Collection<Integer> list = new ArrayList<>(TTTGame.BOARD_WIDTH * TTTGame.BOARD_HEIGHT);

        for (int x = 0; x < TTTGame.BOARD_WIDTH; x++) {
            for (int y = 0; y < TTTGame.BOARD_HEIGHT; y++) {
                TTTGame.Mark m = board[x][y];
                if (m == null) {
                    list.add(-1);
                } else {
                    list.add(m.ordinal());
                }
            }
        }

        return list;
    }

    static TTTGame.Mark[][] deserializeBoard(Collection<Integer> collection) {
        TTTGame.Mark[][] board = new TTTGame.Mark[TTTGame.BOARD_WIDTH][TTTGame.BOARD_HEIGHT];

        Iterator<Integer> iter = collection.iterator();
        for (int x = 0; x < TTTGame.BOARD_WIDTH; x++) {
            for (int y = 0; y < TTTGame.BOARD_HEIGHT; y++) {
                Integer i = iter.next();

                if (i == -1) {
                    board[x][y] = null;
                } else {
                    board[x][y] = TTTGame.Mark.values()[i];
                }
            }
        }

        return board;
    }

}
