package com.litegames.smarty.alpha.game.tictactoe;

import com.litegames.smarty.game.GameState;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TTTGameState implements GameState {

    public enum PlayState {
        PLAYING,
        FINISHED,
    }

    public static class PlayerState {

        public int id;
        public TTTGame.Mark mark;
        public boolean quitted;

        public PlayerState() {
        }

        PlayerState copy() {
            PlayerState copy = new PlayerState();

            copy.id = id;
            copy.mark = mark;
            copy.quitted = quitted;

            return copy;
        }

        @Override
        public String toString() {
            return String.format(Locale.US, "{id: %d, mark: %s, quitted: %b}", id, mark, quitted);
        }
    }

    public PlayerState playersStates[];

    public PlayState playState;
    public TTTGame.Mark currentMark;
    public TTTGame.Mark[][] board;
    public TTTGame.Result result;
    public int winnerId;
    public int quitterId;

    public TTTGameState(List<Integer> playerIds) {
        List<TTTGameState.PlayerState> playersStates = new ArrayList<>();
        for (int i = 0; i < playerIds.size(); i++) {
            TTTGameState.PlayerState playerState = new TTTGameState.PlayerState();
            playerState.id = playerIds.get(i);
            playersStates.add(playerState);
        }

        this.playersStates = playersStates.toArray(new TTTGameState.PlayerState[playersStates.size()]);

        this.playState = PlayState.PLAYING;
        this.currentMark = null;
        this.board = new TTTGame.Mark[TTTGame.BOARD_WIDTH][TTTGame.BOARD_HEIGHT];
        this.result = null;
        this.winnerId = -1;
        this.quitterId = -1;
    }

    public TTTGameState() {

    }

    public GameState copy() {
        TTTGameState copy = new TTTGameState();

        copy.playersStates = new PlayerState[playersStates.length];
        for (int i = 0; i < playersStates.length; ++i) {
            copy.playersStates[i] = playersStates[i].copy();
        }
        copy.playState = playState;
        copy.currentMark = currentMark;
        copy.board = new TTTGame.Mark[TTTGame.BOARD_WIDTH][TTTGame.BOARD_HEIGHT];
        for (int x = 0; x < TTTGame.BOARD_WIDTH; ++x) {
            System.arraycopy(board[x], 0, copy.board[x], 0, TTTGame.BOARD_WIDTH);
        }
        copy.result = result;
        copy.winnerId = winnerId;
        copy.quitterId = quitterId;

        return copy;
    }

    @Override
    public boolean isPlayerQuitter(int playerId) {
        return playerId == quitterId;
    }

    @Override
    public void playerQuit(int playerId) {
        getPlayerState(playerId).quitted = true;

        if (!playState.equals(TTTGameState.PlayState.FINISHED)) {
            result = TTTGame.Result.WIN;

            quitterId = playerId;
            winnerId = getOpponentId(playerId);

            playState = TTTGameState.PlayState.FINISHED;
        }
    }

    @Override
    public boolean hasPlayerQuit(int playerId) {
        return getPlayerState(playerId).quitted;
    }

    @Override
    public boolean isFinished() {
        return playState.equals(PlayState.FINISHED);
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "{playersStates: %s, playState: %s, currentMark: %s, board: %s, result: %s, winnerId: %d, quitterId: %d",
                playersDescription(), playState, currentMark, boardDescription(), result, winnerId, quitterId);
    }

    private String playersDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < playersStates.length; i++) {
            sb.append(playersStates[i]);
            if (i != playersStates.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    private String boardDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int x = 0; x < TTTGame.BOARD_WIDTH; x++) {
            sb.append("[");
            for (int y = 0; y < TTTGame.BOARD_HEIGHT; y++) {
                sb.append(board[x][y]);
                if (y != TTTGame.BOARD_HEIGHT - 1) {
                    sb.append(", ");
                }
            }
            sb.append("]");
            if (x != TTTGame.BOARD_WIDTH - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public int getOpponentId(int playerId) {
        for (PlayerState ps : playersStates) {
            if (ps.id != playerId) {
                return ps.id;
            }
        }

        return -1;
    }

    public int idOfPlayerWithMark(TTTGame.Mark mark) {
        for (PlayerState playersState : playersStates) {
            if (playersState.mark.equals(mark)) {
                return playersState.id;
            }
        }
        return -1;
    }

    public PlayerState getPlayerState(int playerId) {
        for (PlayerState playersState : playersStates) {
            if (playersState.id == playerId) {
                return playersState;
            }
        }
        return null;
    }

    @Override
    public boolean isPlayerWinner(int playerId) {
        if (result.equals(TTTGame.Result.TIE)) {
            return false;
        } else {
            return (playerId == winnerId);
        }
    }

    @Override
    public long getPlayerScore(int playerId) {
        if (result.equals(TTTGame.Result.TIE)) {
            return TTTGame.SCORE_FOR_TIE;
        } else if (playerId == winnerId) {
            return TTTGame.SCORE_FOR_WIN;
        } else {
            return TTTGame.SCORE_FOR_LOSE;
        }
    }

    @Override
    public String getLeaderboardIdentifier() {
        return TTTGame.LEADERBOARD_IDENTIFIER;
    }
}
