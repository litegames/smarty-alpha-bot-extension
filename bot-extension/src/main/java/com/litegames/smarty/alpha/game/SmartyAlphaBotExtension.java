package com.litegames.smarty.alpha.game;

import com.litegames.smarty.alpha.game.tictactoe.TTTAIGamePlayer;
import com.litegames.smarty.alpha.game.tictactoe.TTTGame;
import com.litegames.smarty.alpha.game.tictactoe.TTTGameState;
import com.litegames.smarty.alpha.game.tictactoe.TTTGameStateSerializer;
import com.litegames.smarty.game.Game;
import com.litegames.smarty.game.GameHost;
import com.litegames.smarty.game.GameLocalHost;
import com.litegames.smarty.game.GamePlayer;
import com.litegames.smarty.game.SmartyGameRemoteHost;
import com.litegames.smarty.game.SmartyLocalPlayer;
import com.litegames.smarty.game.SmartyRemotePlayer;
import com.litegames.smarty.sdk.AuthenticationListener;
import com.litegames.smarty.sdk.BotExtension3;
import com.litegames.smarty.sdk.Error;
import com.litegames.smarty.sdk.GameRoom;
import com.litegames.smarty.sdk.Smarty;
import com.litegames.smarty.sdk.User;
import com.litegames.smarty.sdk.UserVariable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class SmartyAlphaBotExtension implements BotExtension3 {

    private static final Integer[] SUPPORTED_GAME_PROTOCOL_VERSIONS = { 0, 1 };

    private static final Logger logger = LoggerFactory.getLogger(SmartyAlphaBotExtension.class);

    private GameHost gameHost;

    @Override
    public String getVersion() {
        return Version.NAME;
    }

    @Override
    public void onInit(Smarty smarty) {
        if (!Arrays.asList(SUPPORTED_GAME_PROTOCOL_VERSIONS).contains(smarty.getGameProtocolVersion())) {
            throw new RuntimeException("Game Protocol version " + smarty.getGameProtocolVersion() + " is not supported");
        }

        // Example of handling new, backward-compatible, fictional 'tie proposals' feature.
        announceSupportedFeatures(smarty);
    }

    @Override
    public void onDestroy() {
    }

    private void announceSupportedFeatures(Smarty smarty) {
        smarty.addAuthenticationListener(new AuthenticationListener() {
            @Override
            public void onLogin(Smarty smarty) {
                List<UserVariable> userVariables = new ArrayList<>();
                userVariables.add(UserVariable.CreateBoolVariable("supportsTieProposals", true));
                smarty.getMySelf().setUserVariables(userVariables);
            }

            @Override
            public void onLoginError(Smarty smarty, Error error) {

            }

            @Override
            public void onLogout(Smarty smarty) {

            }
        });
    }

    @Override
    public void onMatchStart(Smarty smarty, GameRoom gameRoom) {
        boolean meStarts = gameRoom.isMeStarts();

        logger.info("Game start. meStarts: {}", meStarts);

        Collection<User> joinedUsers = gameRoom.getJoinedUsers();

        // Example of handling new, backward-compatible, fictional 'tie proposals' feature.
        {
            boolean supportsTieProposals = true;
            for (User u : joinedUsers) {
                UserVariable uv = u.getUserVariable("supportsTieProposals");
                supportsTieProposals &= uv != null && uv.getBoolValue() != null && uv.getBoolValue();
            }
            logger.info("supportsTieProposals: {}", supportsTieProposals);
        }

        if (meStarts) {
            List<GamePlayer> players = new ArrayList<>();

            for (User user : joinedUsers) {
                if (user.isItMe()) {
                    GamePlayer gamePlayer = new SmartyLocalPlayer(smarty, gameRoom, new TTTAIGamePlayer(user.getId()));
                    players.add(gamePlayer);
                } else {
                    GamePlayer gamePlayer = new SmartyRemotePlayer(smarty, gameRoom, user.getId(), new TTTGameStateSerializer());
                    players.add(gamePlayer);
                }
            }

            Game game = new TTTGame(players);
            gameHost = new GameLocalHost(game, players);
        } else {
            List<Integer> playersIds = new ArrayList<>();
            for (User u : joinedUsers) {
                playersIds.add(u.getId());
            }

            GamePlayer gamePlayer = new TTTAIGamePlayer(smarty.getMySelf().getId());
            TTTGameState gameState = new TTTGameState(playersIds);

            gameHost = new SmartyGameRemoteHost(smarty, gameRoom, gamePlayer, gameState, new TTTGameStateSerializer());
        }

        gameHost.start();
    }

}